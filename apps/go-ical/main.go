package main

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"

	dienste "gitlab.com/Racherom/nz-dienste"

	ical "github.com/Racherom/go-ical"

	"github.com/gorilla/mux"
)

const timeFormat = "01.02.2006"

var baseReq = http.Request{
	Method: http.MethodPost,
	URL: &url.URL{
		Scheme: "https",
		Host:   "local.narrenzunft-gengenbach.de",
		Path:   "/Dienstplan/index.php",
	},
	Header: http.Header{
		"Content-Type": {"application/x-www-form-urlencoded"},
	},
	Proto:      "HTTP/1.1",
	ProtoMajor: 1,
	ProtoMinor: 1,
	Host:       "local.narrenzunft-gengenbach.de",
}

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/nz", func(w http.ResponseWriter, r *http.Request) {
		user, pass, ok := r.BasicAuth()

		if !ok {
			unauthorised(w)
		}

		log.Println("User: ", user)

		now := time.Now()

		form := url.Values{
			"action":   {"servicelist"},
			"von":      {now.Format(timeFormat)},
			"bis":      {now.AddDate(1, 0, 0).Format(timeFormat)},
			"username": {user},
			"password": {pass},
		}

		params := r.URL.Query()

		if cat := params.Get("cat"); cat != "" {
			form.Set("cat", cat)
		} else {
			form.Set("cat", "all")
		}

		if ptype := params.Get("type"); ptype != "" {
			form.Set("type", ptype)
		} else {
			form.Set("type", "M")
		}

		req := baseReq.Clone(context.Background())
		v := strings.NewReader(form.Encode())
		req.Body = ioutil.NopCloser(v)
		req.ContentLength = int64(v.Len())
		snapshot := *v
		req.GetBody = func() (io.ReadCloser, error) {
			r := snapshot
			return ioutil.NopCloser(&r), nil
		}
		log.Println(req.URL.String())
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if resp.StatusCode != http.StatusOK {
			http.Error(w, resp.Status, resp.StatusCode)
			if resp.Body != nil {
				resp.Write(log.Writer())
			}
			return
		}

		log.Println(resp.Status)

		aDienste, err := dienste.ParseDienste(resp.Body)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		cal := ical.VCalendar{
			URL: req.URL.Hostname(),
		}

		for _, d := range aDienste {
			e := ical.VEvent{
				UID:         d.ID,
				Summary:     d.Name,
				Description: d.Tätigkeit,
				Location:    d.Treffpunkt,
			}

			if !d.Beginn.Equal(time.Time{}) {
				e.DTSTART = &d.Beginn
			}

			if !d.Ende.Equal(time.Time{}) {
				e.DTEND = &d.Beginn
			}

			if d.Bemerkung != "" {
				if e.Description != "" {
					e.Description += "\n"
				}
				e.Description += fmt.Sprintf("Bemerkung: %s", d.Bemerkung)
			}
			cal.Events = append(cal.Events, e)
		}

		ical.NewEncoder(w).Encode(cal)

		return
	})

	srv := &http.Server{
		Handler: router,
		Addr:    ":8080",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Println("Start listening...")
	log.Fatal(srv.ListenAndServe())
}

func unauthorised(w http.ResponseWriter) {
	w.Header().Set("WWW-Authenticate", `Basic realm="Narrenzunft Dienste"`)
	w.WriteHeader(401)
	w.Write([]byte("Unauthorised.\n"))
	return
}
