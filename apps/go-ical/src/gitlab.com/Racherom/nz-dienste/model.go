package dienste

import "time"

type Dienst struct {
	ID              string
	Name            string
	Beginn          time.Time
	Ende            time.Time
	Tätigkeit       string
	Ansprechpartner Helfer
	Treffpunkt      string
	ChefVomDienst   Helfer
	Helfer          []Helfer
	Bemerkung       string
}

type Helfer struct {
	ID   int
	Name string
}
