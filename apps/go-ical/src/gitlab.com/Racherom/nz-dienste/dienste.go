package dienste

import (
	"fmt"
	"io"
	"log"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

// ParseDienste ...
func ParseDienste(in io.ReadCloser) ([]Dienst, error) {

	ctx := &html.Node{
		Type:     html.ElementNode,
		DataAtom: atom.Div,
		Data:     "div",
	}
	ns, err := html.ParseFragment(in, ctx)
	if err != nil {
		return nil, fmt.Errorf("Couldn't parse fragment: %v", err)
	}

	var dienste []Dienst
	for i := range ns {
		var dienst Dienst
		doc := goquery.NewDocumentFromNode(ns[i])
		log.Println(doc.Html())
		if id, ok := doc.Attr("id"); ok {
			dienst.ID = id
		}
		var date, start, end string

		address := doc.Find("div.address ul li")
		date = address.Find("span.field_Datum em.eventdatestring").Text()
		start = address.Find("span.field_von").Text()
		end = address.Find("span.field_bis").Text()

		if day, month, year, ok := parseDate(date); ok {
			if hour, minute, ok := parseTime(start); ok {
				dienst.Beginn = time.Date(year, time.Month(month), day, hour, minute, 0, 0, time.Local)
			}

			if hour, minute, ok := parseTime(end); ok {
				dienst.Ende = time.Date(year, time.Month(month), day, hour, minute, 0, 0, time.Local)
			}
		}

		aufbau := doc.Find("div.aufbau ul li")
		dienst.Name = aufbau.Find(".field_Dienst em").Text()
		dienst.Tätigkeit = aufbau.Find(".field_Beschreibung em").Text()

		huber := doc.Find("div.huber ul li")
		dienst.Ansprechpartner = helfer(huber.Find(".dropansprech span em"))
		dienst.Treffpunkt = huber.Find("span.field_Treffpunkt em").Text()

		dienst.ChefVomDienst = helfer(doc.Find("div.chef ul li.dropcfd span em"))

		doc.Find("div.helfer em").Each(func(_ int, s *goquery.Selection) {
			dienst.Helfer = append(dienst.Helfer, helfer(s))
		})

		dienst.Bemerkung = doc.Find("div.field_Bemerkung em").Text()

		dienste = append(dienste, dienst)
	}

	return dienste, nil
}

func helfer(s *goquery.Selection) Helfer {
	id, _ := s.Attr("id")
	return Helfer{
		ID:   helferID(id),
		Name: strings.Trim(s.Text(), ", "),
	}

}

func helferID(s string) int {
	parts := regexp.MustCompile(`[ACH]id_([0-9]+)#?[0-9]*`).FindStringSubmatch(s)
	if len(parts) != 2 {
		return 0
	}
	id, _ := strconv.Atoi(parts[1])
	return id
}

func parseDate(date string) (day int, month int, year int, ok bool) {
	dateParts := strings.Split(date, ".")
	if len(dateParts) != 3 {
		return
	}
	var err error
	if day, err = strconv.Atoi(dateParts[0]); err != nil {
		return
	}

	if month, err = strconv.Atoi(dateParts[1]); err != nil {
		return
	}

	if year, err = strconv.Atoi(dateParts[2]); err != nil {
		return
	}

	ok = true
	return
}

func parseTime(time string) (hour, minutes int, ok bool) {
	dateParts := strings.Split(time, ":")
	if len(dateParts) != 2 {
		return
	}
	var err error
	if hour, err = strconv.Atoi(dateParts[0]); err != nil {
		return
	}

	if minutes, err = strconv.Atoi(dateParts[1]); err != nil {
		return
	}

	ok = true
	return
}
