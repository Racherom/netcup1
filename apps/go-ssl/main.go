package main

import (
	"context"
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"

	"log"

	"github.com/eggsampler/acme"
	"github.com/gorilla/mux"
)

var (
	client    acme.Client
	account   acme.Account
	useEC     = false   // Use EC Key for certificates can be changed with flag --ec
	directory = "/ssl/" // Use different directory then this can't be a parent directory can be changed with flag -d

	tokens     = map[string]acme.Challenge{}
	tokensLock sync.Mutex

	waitGroup = &sync.WaitGroup{}
)

type (
	order struct {
		CN, Path string
		Domains  []string
	}
)

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/.well-known/acme-challenge/{token}", func(w http.ResponseWriter, r *http.Request) {
		token, ok := mux.Vars(r)["token"]
		if !ok || token == "" {
			http.Error(w, "No token found.", 404)
			return
		}

		chal, ok := tokens[token]
		if !ok || chal.KeyAuthorization == "" {
			http.Error(w, "No token found.", 404)
			return
		}

		fmt.Fprint(w, chal.KeyAuthorization)
	})
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r,
			"https://"+r.Host+r.URL.String(),
			http.StatusMovedPermanently)
	})

	var err error

	client, err = acme.NewClient(acme.LetsEncryptProduction)
	if err != nil {
		log.Fatalf("Couldn'n create new acme client: %v", err)
	}

	var privKey *ecdsa.PrivateKey
	if keyData, err := ioutil.ReadFile("/ssl/account.key"); err == nil && len(keyData) > 0 {
		block, _ := pem.Decode(keyData)
		x509Encoded := block.Bytes
		privKey, _ = x509.ParseECPrivateKey(x509Encoded)
	}

	if privKey == nil {
		log.Println("No key found, generating new one.")
		privKey, err = ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
		if err != nil {
			log.Fatalf("Couldn't generate new key: %v", err)
		}
		x509Encoded, err := x509.MarshalECPrivateKey(privKey)
		if err != nil {
			log.Fatalf("Couldn't marschal new key: %v", err)
		}
		f, err := os.Create("/ssl/account.key")
		if err != nil {
			log.Fatalf("Couldn't create new key file: %v", err)
		}

		if err := pem.Encode(f, &pem.Block{Type: "EC PRIVATE KEY", Bytes: x509Encoded}); err != nil {
			f.Close()
			log.Fatalf("Couldn't encode new key: %v", err)
		}

		f.Close()
	}

	if account, err = client.NewAccount(privKey, false, true); err != nil {
		log.Fatalf("Couldn't create acme account: %v", err)
	}

	orders := []order{}

	if f, err := os.Open("domains.json"); err != nil {
		log.Fatalf("Couldn't open domains.json: %v", err)
	} else {
		if err := json.NewDecoder(f).Decode(&orders); err != nil {
			log.Fatalf("Couldn'n decode domains.json: %v", err)
		}
	}

	log.Printf("Orders: %#v", orders)

	srv := &http.Server{
		Handler: r,
		Addr:    ":8080",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	ch := make(chan os.Signal)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)

	log.Println("Start server")
	go func() {
		if err := srv.ListenAndServe(); err != http.ErrServerClosed {
			log.Fatalf("ListenAndServe(): %v", err)
		}
	}()

	ticker := time.NewTicker(5 * time.Minute)
	for i := range orders {
		go func(o order) {
			waitGroup.Add(1)
			defer waitGroup.Done()
			err := handleOrder(o)
			if err != nil {
				log.Printf("Couldn't handle order %s: %v", o.CN, err)
			}
		}(orders[i])
	}
	var i int
Loop:
	for {
		select {
		case <-ticker.C:
			waitGroup.Add(1)

			if err := handleOrder(orders[i]); err != nil {
				log.Printf("Couldn't handle order %s: %v", orders[i].CN, err)
			}

			waitGroup.Done()
			i++
			if i >= len(orders) {
				i = 0
			}
		case <-ch:
			ticker.Stop()
			break Loop
		}
	}

	waitGroup.Wait()
	err = srv.Shutdown(context.Background())
	if err != nil {
		log.Fatalf("Couldn't shutdown server %v", err)
	}
}

func handleOrder(order order) error {
	cert := getExistingCert(order)
	if cert != nil && cert.Leaf != nil {
		if math.Round(cert.Leaf.NotAfter.Sub(time.Now()).Hours()/24) > 30 && exist(cert.Leaf.DNSNames, append(order.Domains, order.CN)...) {
			return nil
		}
	}

	acmeOrder, err := client.NewOrderDomains(account, append(order.Domains, order.CN)...)
	if err != nil {
		return err
	}

	for _, authURL := range acmeOrder.Authorizations {
		auth, err := client.FetchAuthorization(account, authURL)
		if err != nil {
			return fmt.Errorf("Error fetching authorization Url %q: %v ", authURL, err)
		}

		if auth.Status == "valid" {
			continue
		}

		chal, ok := auth.ChallengeMap[acme.ChallengeTypeHTTP01]
		if !ok {
			return fmt.Errorf("Unable to find http-01 challenge for auth %s, Url: %s ", auth.Identifier.Value, authURL)
		}

		tokens[chal.Token] = chal
		chal, err = client.UpdateChallenge(account, chal)
		if err != nil {
			return fmt.Errorf("Error updating authorization %s challenge (Url: %s) : %v ", auth.Identifier.Value, authURL, err)
		}
	}

	// generate private key for cert
	var certSinger crypto.Signer
	var keyBlock *pem.Block

	certKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return fmt.Errorf("Error generating certificate key for %s: %v ", order.Domains, err)
	}
	certSinger = certKey
	certKeyEnc := x509.MarshalPKCS1PrivateKey(certKey)
	keyBlock = &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: certKeyEnc,
	}

	certKeyPem := pem.EncodeToMemory(keyBlock)

	// create the new csr template
	tpl := &x509.CertificateRequest{
		PublicKey: certSinger.Public(),
		Subject:   pkix.Name{CommonName: order.CN},
		DNSNames:  order.Domains,
	}
	csrDer, err := x509.CreateCertificateRequest(rand.Reader, tpl, certSinger)
	if err != nil {
		return fmt.Errorf("Error creating certificate request for %s: %v ", order.Domains, err)
	}
	csr, err := x509.ParseCertificateRequest(csrDer)
	if err != nil {
		return fmt.Errorf("Error parsing certificate request for %s: %v ", order.Domains, err)
	}

	// finalize the order with the acme server given a csr
	acmeOrder, err = client.FinalizeOrder(account, acmeOrder, csr)
	if err != nil {
		return fmt.Errorf("Error finalizing order for %s: %v ", order.Domains, err)
	}

	// fetch the certificate chain from the finalized order provided by the acme server
	certs, err := client.FetchCertificates(account, acmeOrder.Certificate)
	if err != nil {
		return fmt.Errorf("Error fetching certificates for %v: %v ", order.Domains, err)
	}

	f, err := os.Create(order.Path)
	if err != nil {
		return fmt.Errorf("Couldn't create new cert file: %v", err)
	}

	defer f.Close()

	f.Write(certKeyPem)
	for _, c := range certs {
		pem.Encode(f, &pem.Block{
			Type:  "CERTIFICATE",
			Bytes: c.Raw,
		})
	}

	return nil
}

func getExistingCert(order order) *tls.Certificate {
	rawCert, err := ioutil.ReadFile(order.Path)
	if err == nil && len(rawCert) > 0 {
		privBlock, pubData := pem.Decode(rawCert)
		if len(pubData) == 0 {
			// no public key data (cert/issuer), ignore
			return nil
		}

		// decode pub chain
		var pubDER [][]byte
		var pub []byte
		for len(pubData) > 0 {
			var b *pem.Block
			b, pubData = pem.Decode(pubData)
			if b == nil {
				break
			}
			pubDER = append(pubDER, b.Bytes)
			pub = append(pub, b.Bytes...)
		}
		if len(pubData) > 0 {
			// leftover data in file - possibly corrupt, ignore
			return nil
		}

		certs, err := x509.ParseCertificates(pub)
		if err != nil {
			// bad certificates, ignore
			return nil
		}

		leaf := certs[0]

		// add any intermediate certs if present
		var intermediates *x509.CertPool
		if len(certs) > 1 {
			intermediates = x509.NewCertPool()
			for i := 1; i < len(certs); i++ {
				intermediates.AddCert(certs[i])
			}
		}

		for _, domain := range order.Domains {
			if _, err := leaf.Verify(x509.VerifyOptions{DNSName: domain, Intermediates: intermediates}); err != nil {
				// invalid certificates , ignore
				return nil
			}
		}

		var privKey crypto.PrivateKey
		if privBlock != nil {
			if strings.Contains(privBlock.Type, "EC") {
				privKey, _ = x509.ParseECPrivateKey(privBlock.Bytes)
			} else {
				privKey, _ = x509.ParsePKCS1PrivateKey(privBlock.Bytes)
			}

		}
		return &tls.Certificate{
			Certificate: pubDER,
			PrivateKey:  privKey,
			Leaf:        leaf,
		}
	}
	return nil

}

func exist(slice []string, values ...string) bool {
	for _, value := range values {
		exist := false
		for _, entry := range slice {
			if value == entry {
				exist = true
				break
			}
		}
		if !exist {
			return false
		}
	}
	return true
}
